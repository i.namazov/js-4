let tabs = document.querySelectorAll('.tabs-title')
let content = document.querySelectorAll('.con-items')
for(let i = 0;i<tabs.length;i++){
    tabs[i].addEventListener("click",()=>{
        for (let j=0; j<tabs.length; j++){
            tabs[j].classList.remove("active");
            content[j].style.display = "none";
        }
        tabs[i].classList.add("active");
        content[i].style.display="block";
    })
}